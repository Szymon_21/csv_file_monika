from logging import raiseExceptions
import sys
from exception import FileExtensionException
import csv


class FilterData:
    
    def __init__(self, input_file_name, output_file_name, column, value_from, value_to):

        self.input_file_name = input_file_name
        self.output_file_name = output_file_name
        self.column = int(column)
        self.value_from = float(value_from)
        self.value_to = float(value_to)

        self.input_data = []
        self.output_data = []

    def read_file(self):
        with open(self.input_file_name, "r") as file:
            for line in file:
                self.input_data.append(
                    line.replace("\n", "").split(",")
                )
    
    def filter_data(self):
        for row in self.input_data:

            if float(row[self.column - 1]) >= self.value_from and float(row[self.column - 1]) <= self.value_to:
                self.output_data.append(row)
    
    def save_to_file(self):
        with open(self.output_file_name, "w") as file:
            writer = csv.writer(file)        
            for row in self.output_data:
                writer.writerow(row)
    
    def process(self):
        self.read_file()
        self.filter_data()
        self.save_to_file()
                

def validate(argv):
    if len(argv) < 5:
        raise Exception("Za mało parametrów")
    elif len(argv) > 6:
        raise Exception("Za dużo parametrów")
     
    try:
        int(argv[3])
    except ValueError:
        raise ValueError("Nie jest intem")
    
    if argv[1].split(".")[1] != "csv" and argv[2].split(".")[1] != "csv":
        raise FileExtensionException("Pliki nie są csv")

def parser(argv):
    validate(argv)

    if len(argv) != 6:
        return (argv[1], argv[2], argv[3], argv[4], None)   
    return (argv[1], argv[2], argv[3], argv[4], argv[5])


input_file_name, output_file_name, column, value_from, value_to = parser(sys.argv)

filter = FilterData(input_file_name, output_file_name, column, value_from, value_to)

filter.process()